Title:
Export To Excel from ASP with VB Script


Author: 

Chad Compton
Chad Compton Designs
http://www.chadcompton.com


Date:

9/13/2013


Purpose:

To export a table from any source to an excel file, from classic asp using VB Script.
Asp.net makes it easy, but there was a trick to using classic asp for this task.
This is what made me decide to make this git public for anyone to use.
I hope this helps you out in some way.


Included:

Sample Access database (Emp)
View.asp page to view the table
ExportToExcel.asp which writes the excel page



