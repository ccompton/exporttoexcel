
<%@ Language=VBScript %>
<%
dim Cn,Rs

set Cn=server.createobject("ADODB.connection")
set Rs=server.createobject("ADODB.recordset")

'This is just your connection to any type of database you are using.
Cn.open "provider=microsoft.jet.oledb.4.0;data source=" & server.mappath("Sample.mdb")

Rs.open "select * from Emp",Cn,1,3

Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=ExportList.xls"

if Rs.eof <> true then

	response.write "<table border=1 cellspacing=0 width=400>"
	
	'This part will add the Header to your excel file.
	response.write "<tr><th>Id</th><th>Name</th><th>Address</th><th>Description</th></tr>"
	
	while not Rs.eof
		response.write "<tr><td>" & Rs.fields("ID") & "</td><td>" 
		response.write Rs.fields("Name") & "</td><td>" & Rs.fields("Address") & "</td><td>" & Rs.fields("Descr") & "</td></tr>"
		Rs.movenext
	wend
	
	response.write "</table>"
	
end if

set rs=nothing

Cn.close
%>
